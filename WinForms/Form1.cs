﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace WinForms
{
    public partial class Form1 : Form
    {
        bool paint;
        int x, y, lx, ly; //Координаты для отрисовки линий, прямоугольников, кругов
        Color color = Color.Black;

        enum Instruments {Pencil, Line, Rectangle, Ellipse} //Перечисление инструментов
        Instruments currInstrument;

        public Form1()
        {
            InitializeComponent();
        }

        //Функция для удобства переключения кнопок
        private void SetEquipment(Instruments buttonType, bool setColorButtonState)
        {
            currInstrument = buttonType;
            buttonColor.Enabled = setColorButtonState;
        }

        //Очистка области рисования
        private void ButtonClearClick(object sender, EventArgs e)
        {
            Graphics g1 = picturePaint.CreateGraphics();
            g1.Clear(picturePaint.BackColor);
        }

        private void ButtonPencilClick(object sender, EventArgs e)
        {
            SetEquipment(Instruments.Pencil, false);
        }

        private void ButtonLineClick(object sender, EventArgs e)
        {
            SetEquipment(Instruments.Line, false);
        }

        private void ButtonRectangleClick(object sender, EventArgs e)
        {
            SetEquipment(Instruments.Rectangle, true);
        }

        private void ButtonEllipseClick(object sender, EventArgs e)
        {
            SetEquipment(Instruments.Ellipse, true);
        }

        //Загрузка картинки из файла
        private void ButtonOpenClick(object sender, EventArgs e)
        {
            var o = new OpenFileDialog { Filter = "PNG|*.png|JPG|*.jpg|BMP|*.bmp" }; //Фильтры для трёх видов картинок
            if (o.ShowDialog() == DialogResult.OK) //Если на форме нажата кнопка ОК
            {
                picturePaint.Image = (Image)Image.FromFile(o.FileName).Clone(); //Загружаем на pictureBox картинку из файла
            }
        }

        //Сохранение картинки в файл
        private void ButtonSaveClick(object sender, EventArgs e)
        {
            var bmp = new Bitmap(picturePaint.Width, picturePaint.Height); //Создаём битмап для выделения области картинки
            Graphics g = Graphics.FromImage(bmp);
            Rectangle rect = picturePaint.RectangleToScreen(picturePaint.ClientRectangle);
            g.CopyFromScreen(rect.Location, Point.Empty, picturePaint.Size); //Делаем "скриншот" картинки из pictureBox
            g.Dispose();
            var s = new SaveFileDialog { Filter = "PNG|*.png|JPG|*.jpg|BMP|*.bmp" };
            if (s.ShowDialog() == DialogResult.OK) //Если на форме нажата кнопка ОК
            {
                if (File.Exists(s.FileName)) //Если файл существует, удаляем старый вариант
                {
                    File.Delete(s.FileName);
                }
                //Проверки на соответствие конкретному типу
                if (s.FileName.Contains(".png"))
                {
                    bmp.Save(s.FileName, ImageFormat.Png);
                }
                else if (s.FileName.Contains(".jpg"))
                {
                    bmp.Save(s.FileName, ImageFormat.Jpeg);
                }
                else if (s.FileName.Contains(".bmp"))
                {
                    bmp.Save(s.FileName, ImageFormat.Bmp);
                }
            }
        }

        //По нажатию кнопки разрешаем рисовани и считываем координаты мышки
        private void PicturePaintMouseDown(object sender, MouseEventArgs e)
        {
            paint = true;
            x = e.X;
            y = e.Y;
        }

        //По отжатию кнопки запрещаем рисование и считываем конечные координаты мышки(для отрисовки линии)
        private void PicturePaintMouseUp(object sender, MouseEventArgs e)
        {
            paint = false;
            lx = e.X;
            ly = e.Y;
            if (currInstrument == Instruments.Line)
            {
                Graphics g = picturePaint.CreateGraphics();
                g.DrawLine(new Pen(new SolidBrush(Color.Black)), new Point(x, y), new Point(lx, ly));
                g.Dispose();
            }
        }

        //По движению мышки по pictureBox`у в зависимости от выбраного инструмента рисуем нужные фигуры
        private void PicturePaintMouseMove(object sender, MouseEventArgs e)
        {
            if (paint)
            {
                Graphics g = picturePaint.CreateGraphics();
                switch (currInstrument)
                {
                    case Instruments.Pencil:
                        g.FillEllipse(new SolidBrush(Color.Black), e.X, e.Y, 5, 5);
                        g.Dispose();
                        break;
                    case Instruments.Rectangle:
                        g.FillRectangle(new SolidBrush(color), x, y, e.X - x, e.Y - y);
                        g.Dispose();
                        break;
                    case Instruments.Ellipse:
                        g.FillEllipse(new SolidBrush(color), x, y, e.X - x, e.Y - y);
                        g.Dispose();
                        break;
                }
            }
        }

        private void PicturePaintClick(object sender, EventArgs e)
        {

        }

        //Вывод формы с выбором цвета
        private void ButtonColorClick(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                color = colorDialog.Color;
            }
        }

    }
}

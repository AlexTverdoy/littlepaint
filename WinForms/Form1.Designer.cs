﻿namespace WinForms
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonClear = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonPencil = new System.Windows.Forms.Button();
            this.buttonLine = new System.Windows.Forms.Button();
            this.buttonEllipse = new System.Windows.Forms.Button();
            this.buttonRectangle = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.picturePaint = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buttonColor = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturePaint)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(12, 426);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 0;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonPencil);
            this.groupBox1.Controls.Add(this.buttonLine);
            this.groupBox1.Controls.Add(this.buttonEllipse);
            this.groupBox1.Controls.Add(this.buttonRectangle);
            this.groupBox1.Location = new System.Drawing.Point(12, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(75, 338);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Instruments";
            // 
            // buttonPencil
            // 
            this.buttonPencil.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPencil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPencil.Image = global::WinForms.Properties.Resources.black_pencil_vector_KTnezEc;
            this.buttonPencil.Location = new System.Drawing.Point(14, 58);
            this.buttonPencil.Name = "buttonPencil";
            this.buttonPencil.Size = new System.Drawing.Size(45, 45);
            this.buttonPencil.TabIndex = 1;
            this.buttonPencil.UseVisualStyleBackColor = false;
            this.buttonPencil.Click += new System.EventHandler(this.ButtonPencilClick);
            // 
            // buttonLine
            // 
            this.buttonLine.Image = global::WinForms.Properties.Resources.line;
            this.buttonLine.Location = new System.Drawing.Point(14, 109);
            this.buttonLine.Name = "buttonLine";
            this.buttonLine.Size = new System.Drawing.Size(45, 45);
            this.buttonLine.TabIndex = 2;
            this.buttonLine.UseVisualStyleBackColor = true;
            this.buttonLine.Click += new System.EventHandler(this.ButtonLineClick);
            // 
            // buttonEllipse
            // 
            this.buttonEllipse.Image = global::WinForms.Properties.Resources._1185346159_pic10000;
            this.buttonEllipse.Location = new System.Drawing.Point(14, 211);
            this.buttonEllipse.Name = "buttonEllipse";
            this.buttonEllipse.Size = new System.Drawing.Size(45, 45);
            this.buttonEllipse.TabIndex = 4;
            this.buttonEllipse.UseVisualStyleBackColor = true;
            this.buttonEllipse.Click += new System.EventHandler(this.ButtonEllipseClick);
            // 
            // buttonRectangle
            // 
            this.buttonRectangle.Image = global::WinForms.Properties.Resources._3;
            this.buttonRectangle.Location = new System.Drawing.Point(14, 160);
            this.buttonRectangle.Name = "buttonRectangle";
            this.buttonRectangle.Size = new System.Drawing.Size(45, 45);
            this.buttonRectangle.TabIndex = 3;
            this.buttonRectangle.UseVisualStyleBackColor = true;
            this.buttonRectangle.Click += new System.EventHandler(this.ButtonRectangleClick);
            // 
            // buttonSave
            // 
            this.buttonSave.Image = global::WinForms.Properties.Resources.document_save;
            this.buttonSave.Location = new System.Drawing.Point(52, 12);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(35, 35);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSaveClick);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Image = global::WinForms.Properties.Resources.Open_Folder;
            this.buttonOpen.Location = new System.Drawing.Point(12, 12);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(35, 35);
            this.buttonOpen.TabIndex = 1;
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.ButtonOpenClick);
            // 
            // picturePaint
            // 
            this.picturePaint.BackColor = System.Drawing.Color.White;
            this.picturePaint.Location = new System.Drawing.Point(93, 12);
            this.picturePaint.Name = "picturePaint";
            this.picturePaint.Size = new System.Drawing.Size(646, 437);
            this.picturePaint.TabIndex = 3;
            this.picturePaint.TabStop = false;
            this.picturePaint.Click += new System.EventHandler(this.PicturePaintClick);
            this.picturePaint.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicturePaintMouseDown);
            this.picturePaint.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PicturePaintMouseMove);
            this.picturePaint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicturePaintMouseUp);
            // 
            // buttonColor
            // 
            this.buttonColor.Enabled = false;
            this.buttonColor.Location = new System.Drawing.Point(12, 397);
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.Size = new System.Drawing.Size(75, 23);
            this.buttonColor.TabIndex = 4;
            this.buttonColor.Text = "Color";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.ButtonColorClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 461);
            this.Controls.Add(this.buttonColor);
            this.Controls.Add(this.picturePaint);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonClear);
            this.Name = "Form1";
            this.Text = "LittlePaint";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picturePaint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonPencil;
        private System.Windows.Forms.Button buttonLine;
        private System.Windows.Forms.Button buttonEllipse;
        private System.Windows.Forms.Button buttonRectangle;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.PictureBox picturePaint;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buttonColor;

    }
}

